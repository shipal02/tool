def call(String repoUrl) {
     pipeline {
          agent any
           tools {
               ansible 'ansible'
               }
               stages {
                    stage('Clone') {
                         steps {
                              git branch: 'main',
                                   url: "$repoUrl"
                         }
                    }
                    stage('Ansible') {
                         steps {
                              ansiblePlaybook(inventory: '',installation: 'ansible',limit: '',playbook: '/home/ubuntu/jenkins/workspace/tool_playbook/roles/mongorole.yml',extras: '')
                         }
                    }
               }
     }
}
